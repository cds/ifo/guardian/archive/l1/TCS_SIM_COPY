from guardian import GuardState, GuardStateDecorator

# initial request on initialization
request = 'COPY'
# nominal operating state
nominal = 'COPY'

####################
# STATES
####################

class INIT(GuardState):
    request = False
    def main(self):
        # turn down ezca write logging
        def logput(pv, value):
            message = "%s => %s" % (pv.pvname, value)
            ezca._log.debug(message)
        ezca._logput = logput
        return 'COPY'


class COPY(GuardState):
    index = 10
    request = True
    def main(self):
        self.timer._logfunc = None
        self.timer['wait'] = 1

    def run(self):
        if not self.timer['wait']:
            return True
        ezca['TCS-SET_ITMX_CO2_LASERPOWER_POWER_REQUEST'] = ezca['TCS-ITMX_CO2_LASERPOWER_POWER_REQUEST']
        ezca['TCS-SET_ITMY_CO2_LASERPOWER_POWER_REQUEST'] = ezca['TCS-ITMY_CO2_LASERPOWER_POWER_REQUEST']
        ezca['TCS-SET_ITMX_CO2_LASERPOWER_ANGLE_REQUEST'] = ezca['TCS-ITMX_CO2_LASERPOWER_ANGLE_REQUEST']
        ezca['TCS-SET_ITMY_CO2_LASERPOWER_ANGLE_REQUEST'] = ezca['TCS-ITMY_CO2_LASERPOWER_ANGLE_REQUEST']
        ezca['TCS-SET_ITMX_CO2_LASERPOWER_ANGLE_CALC'] = ezca['TCS-ITMX_CO2_LASERPOWER_ANGLE_CALC']
        ezca['TCS-SET_ITMY_CO2_LASERPOWER_ANGLE_CALC'] = ezca['TCS-ITMY_CO2_LASERPOWER_ANGLE_CALC']
        ezca['TCS-SET_ITMX_RH_UPPERPOWER'] = ezca['TCS-ITMX_RH_UPPERPOWER']
        ezca['TCS-SET_ITMX_RH_LOWERPOWER'] = ezca['TCS-ITMX_RH_LOWERPOWER']
        ezca['TCS-SET_ITMY_RH_UPPERPOWER'] = ezca['TCS-ITMY_RH_UPPERPOWER']
        ezca['TCS-SET_ITMY_RH_LOWERPOWER'] = ezca['TCS-ITMY_RH_LOWERPOWER']
        ezca['TCS-SET_ETMX_RH_UPPERPOWER'] = ezca['TCS-ETMX_RH_UPPERPOWER']
        ezca['TCS-SET_ETMX_RH_LOWERPOWER'] = ezca['TCS-ETMX_RH_LOWERPOWER']
        ezca['TCS-SET_ETMY_RH_UPPERPOWER'] = ezca['TCS-ETMY_RH_UPPERPOWER']
        ezca['TCS-SET_ETMY_RH_LOWERPOWER'] = ezca['TCS-ETMY_RH_LOWERPOWER']
        ezca['TCS-SET_ITMX_CO2_LSRPWR_MTR_OUTPUT'] = ezca['TCS-ITMX_CO2_LSRPWR_MTR_OUTPUT']
        ezca['TCS-SET_ITMY_CO2_LSRPWR_MTR_OUTPUT'] = ezca['TCS-ITMY_CO2_LSRPWR_MTR_OUTPUT']
        ezca['TCS-SET_X_TR_B_SUM_OUTPUT'] = ezca['ASC-X_TR_B_SUM_OUTPUT']
        ezca['TCS-SET_Y_TR_B_SUM_OUTPUT'] = ezca['ASC-Y_TR_B_SUM_OUTPUT']
        self.timer['wait'] = 1
        return True



edges = [
    ('INIT','COPY'),
    ]

